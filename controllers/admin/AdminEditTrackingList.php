<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

include_once _PS_MODULE_DIR_ . 'edittrackinglist/classes/ETL.php';

class AdminEditTrackingListController extends ModuleAdminController
{
    protected $position_identifier = 'id_edittrackinglist';

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'edittrackinglist';
        $this->identifier = 'id_edittrackinglist';
        $this->name = 'edittrackinglist';
        $this->className = 'ETL';
        $this->lang = false;
        $this->deleted = false;
        $this->colorOnBackground = false;
        $this->bulk_actions = array(
            'delete' => array('text' => 'Delete selected', 'confirm' => 'Delete selected items?'),
        );
        $this->context = Context::getContext();

        $this->_defaultOrderBy = 'position';

        parent::__construct();
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        $this->addJS(_PS_MODULE_DIR_ . 'edittrackinglist/views/js/render_list.js');
    }

    public function renderList()
    {
        $this->fields_list = array(
            'position' => array(
                'title' => $this->l('Position'),
                'align' => 'text-center',
                'position' => 'position',
                'filter_key' => 'position',
                'class' => 'fixed-width-xs',
                'search' => false,
                'remove_onclick' => true
            ),
            'name_display' => array(
                'title' => $this->l('Name'),
                'align' => 'text-center',
                'search' => false,
                'remove_onclick' => true
            ),
            'active' => array(
                'title' => $this->l('Display'),
                'type' => 'bool',
                'search' => false,
                'callback' => 'printActiveIcon',
                'remove_onclick' => true
            ),
            'title' => array(
                'title' => $this->l('Title'),
                'align' => 'text-center',
                'search' => false,
                'callback' => 'printTitleInput',
                'remove_onclick' => true
            ),
            'mail' => array(
                'title' => $this->l('Email'),
                'align' => 'text-center',
                'search' => false,
                'callback' => 'printMailInput',
                'remove_onclick' => true
            )
        );

        $script = "<script>
            $('document').ready(function() {
                $('#edittrackinglist_form_submit_btn').show();
            })</script>";

        $return = '';
        if (version_compare(_PS_VERSION_, '1.7.0', '<')) {
            $return = $this->initForm();
        }

        return parent::renderList() . $return . $script;
    }

    public function initForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitETL';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminEditTrackingList', false);
        $helper->token = Tools::getAdminTokenLite('AdminEditTrackingList');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(),
            'languages' => $this->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    public function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Recalculate the shipping price on carrier change?'),
                        'name' => 'PS_ORDER_RECALCULATE_SHIPPING',
                        'values' => array(
                            array(
                                'id' => 'active_recalculate',
                                'value' => true
                            ),
                            array(
                                'id' => 'inactive_recalculate',
                                'value' => false
                            )
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                ),
            ),
        );
    }

    public function getConfigFormValues()
    {
        return array(
            'PS_ORDER_RECALCULATE_SHIPPING' => (int)Configuration::get('PS_ORDER_RECALCULATE_SHIPPING')
        );
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitETL') && version_compare(_PS_VERSION_, '1.7.0', '<')) {
            Configuration::updateValue(
                'PS_ORDER_RECALCULATE_SHIPPING',
                Tools::getValue('PS_ORDER_RECALCULATE_SHIPPING')
            );
        }

        return parent::postProcess();
    }

    public function printMailInput($value, $tr)
    {
        $id = (int)$tr['id_edittrackinglist'];

        if ($tr['name'] == 'delivered') {
            $value = Db::getInstance()->getValue(
                "SELECT `send_email`
                FROM `" . _DB_PREFIX_ . "order_state`
                WHERE `id_order_state` = 5"
            );
        }

        $this->context->smarty->assign(array(
            'id' => (int)$id,
            'value' => $value,
            'name' => $tr['name']
        ));

        return $this->context->smarty->fetch($this->getTemplatePath() . 'select_mail_field.tpl');
    }

    public function printTitleInput($value, $tr)
    {
        $this->context->smarty->assign(array(
            'id' => (int)$tr['id_edittrackinglist'],
            'value' => $value
        ));
        return $this->context->smarty->fetch($this->getTemplatePath() . 'text_title_field.tpl');
    }

    public function printActiveIcon($value, $tr)
    {
        $this->context->smarty->assign(array(
            'id' => $tr['id_edittrackinglist'],
            'value' => $value,
            'token' => Tools::getAdminTokenLite('AdminEditTrackingList')
        ));
        return $this->context->smarty->fetch($this->getTemplatePath() . 'boolean_display_field.tpl');
    }

    public function processChangeActive()
    {
        $id = (int)Tools::getValue('id');
        $etl = new ETL($id);

        if (!Validate::isLoadedObject($etl)) {
            $this->errors[] = $this->l('An error has occurred.');
            return false;
        }

        $etl->active = (int)!$etl->active;
        $etl->update();

        return true;
    }

    public function ajaxProcessChangeTitle()
    {
        $title = Tools::getValue('value');

        if (empty($title)) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('Please fill the title field.')
            )));
        }

        $id = (int)Tools::getValue('id');
        $etl = new ETL($id);

        if (!Validate::isLoadedObject($etl)) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('An error has occurred.')
            )));
        }

        $etl->title = $title;
        $etl->update();

        die(json_encode(array(
            'success' => 1
        )));
    }

    public function ajaxProcessChangeMail()
    {
        $mail = Tools::getValue('value');

        if (!Validate::isUnsignedInt($mail) || $mail > 2) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('The value is not correct.')
            )));
        }

        $id = (int)Tools::getValue('id');
        $etl = new ETL($id);

        if (!Validate::isLoadedObject($etl)) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('An error has occurred.')
            )));
        }

        $order_state = new OrderState(5);

        if (!Validate::isLoadedObject($order_state)) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('An error has occurred while loading the order state.')
            )));
        } elseif ($etl->name == 'delivered' && $mail == 2) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('An error has occurred.')
            )));
        } elseif ($etl->name == 'delivered' && $mail == 1) {
            $has_template = true;
            foreach ($order_state->template as $template) {
                if (empty($template)) {
                    $has_template = false;
                    break;
                }
            }

            if (!$has_template) {
                die(json_encode(array(
                    'success' => 0,
                    'text' => $this->l('You didn\'t select any template. Please go to "Shop parameters -> Orders settings -> statuses" and select a template for the line "Delivered"')
                )));
            }
        }

        if ($etl->name == 'delivered') {
            $order_state->send_email = $mail;
            $order_state->update();
        }

        $etl->mail = $mail;
        $etl->update();

        die(json_encode(array(
            'success' => 1
        )));
    }

    public function ajaxProcessChangeCarrier()
    {
        $id_order_carrier = (int)Tools::getValue('id_order_carrier');
        $id_carrier = (int)Tools::getValue('value');

        if (!ObjectModel::existsInDatabase($id_carrier, 'carrier')) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('This carrier does not exist.')
            )));
        }

        $order_carrier = new OrderCarrier($id_order_carrier);

        $id_order = (int)$order_carrier->id_order;
        $order = new Order($id_order);

        if (!Validate::isLoadedObject($order) || !Validate::isLoadedObject($order_carrier)) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('The order does not exist.')
            )));
        }

        $order_carrier->id_carrier = $id_carrier;
        $order_carrier->update();

        if (!ETL::refreshShippingCost($order, $order_carrier)) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('An error has occurred while changing the price of the shipping.')
            )));
        }

        if ((bool)(int)Tools::getValue('send_mail')) {
            if (!$this->sendInTransitEmail($order_carrier, $order)) {
                die(json_encode(array(
                    'success' => 0,
                    'text' => $this->l('An error has occurred while sending the mail.')
                )));
            }
        }

        die(json_encode(array(
            'success' => 1
        )));
    }

    public function ajaxProcessChangeTracking()
    {
        $id_order_carrier = (int)Tools::getValue('id_order_carrier');
        $tracking = Tools::getValue('value');

        if (!Validate::isTrackingNumber($tracking)) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('The tracking number is not valid.')
            )));
        }

        $order_carrier = new OrderCarrier($id_order_carrier);
        $order = new Order($order_carrier->id_order);

        if (!Validate::isLoadedObject($order_carrier) || !Validate::isLoadedObject($order)) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('The order does not exist.')
            )));
        }

        $order_carrier->tracking_number = $tracking;
        $order_carrier->update();

        if ((bool)(int)Tools::getValue('send_mail')) {
            if (!$this->sendInTransitEmail($order_carrier, $order)) {
                die(json_encode(array(
                    'success' => 0,
                    'text' => $this->l('An error has occurred while sending the mail.')
                )));
            }
        }
        die(json_encode(array(
            'success' => 1
        )));
    }

    public function processChangeDelivered()
    {
        $id_order_state = 5;
        $id_order = (int)Tools::getValue('id_order');

        $order = new Order($id_order);
        $order_state = new OrderState($id_order_state);

        if (!Validate::isLoadedObject($order_state) || !Validate::isLoadedObject($order)) {
            $this->errors[] = $this->l('The new order status is invalid.');
        } else {
            $current_order_state = $order->getCurrentOrderState();
            if ($current_order_state->id != $order_state->id) {
                // Create new OrderHistory
                $history = new OrderHistory();
                $history->id_order = $order->id;
                $history->id_employee = (int) $this->context->employee->id;

                $use_existings_payment = false;
                if (!$order->hasInvoice()) {
                    $use_existings_payment = true;
                }
                $history->changeIdOrderState((int) $order_state->id, $order, $use_existings_payment);

                $carrier = new Carrier($order->id_carrier, $order->id_lang);
                $templateVars = array();
                if ($history->id_order_state == Configuration::get('PS_OS_SHIPPING') && $order->shipping_number) {
                    $templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));
                }

                // Save all changes
                if ($history->addWithemail(true, $templateVars)) {
                    // synchronizes quantities if needed..
                    if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                        foreach ($order->getProducts() as $product) {
                            if (StockAvailable::dependsOnStock($product['product_id'])) {
                                StockAvailable::synchronize($product['product_id'], (int) $product['id_shop']);
                            }
                        }
                    }

                    $link = $this->context->link->getAdminLink('AdminOrders', true);
                    $link .= '&conf=4';

                    Tools::redirectAdmin($link);
                }
                $this->errors[] = $this->l('An error occurred while changing order status, or we were unable to send an email to the customer.');
            } else {
                $this->errors[] = $this->l('The order has already been assigned this status.');
            }
        }
    }

    public function ajaxProcessUpdatePositions()
    {
        $way = (int)Tools::getValue('way');
        $id_edittrackinglist = (int)Tools::getValue('id');
        $positions = Tools::getValue('edittrackinglist');

        $new_positions = array();
        foreach ($positions as $v) {
            if (count(explode('_', $v)) == 4) {
                $new_positions[] = $v;
            }
        }

        foreach ($new_positions as $position => $value) {
            $pos = explode('_', $value);

            if (isset($pos[2]) && (int)$pos[2] === $id_edittrackinglist) {
                if ($etl = new ETL()) {
                    Configuration::updateValue('id_edittrackinglist', $id_edittrackinglist);
                    if (isset($position) && $etl->updatePosition($way, $position)) {
                        echo 'ok position '.(int)$position.' for id '.(int)$pos[1].'\r\n';
                    } else {
                        echo '{"hasError" : true, "errors" : "Can not update id ' . (int)$id_edittrackinglist .
                            ' to position ' . (int)$position . ' "}';
                    }
                }

                break;
            }
        }
    }

    public function sendInTransitEmail($order_carrier, $order)
    {
        $customer = new Customer((int) $order->id_customer);
        $carrier = new Carrier((int) $order_carrier->id_carrier, $order->id_lang);
        $address = new Address((int) $order->id_address_delivery);

        $templateVars = array(
            '{followup}' => str_replace('@', $order_carrier->tracking_number, $carrier->url),
            '{firstname}' => $customer->firstname,
            '{lastname}' => $customer->lastname,
            '{id_order}' => $order->id,
            '{shipping_number}' => $order_carrier->tracking_number,
            '{order_name}' => $order->getUniqReference(),
            '{carrier}' => $carrier->name,
            '{address1}' => $address->address1,
            '{country}' => $address->country,
            '{postcode}' => $address->postcode,
            '{city}' => $address->city
        );

        if (Mail::Send(
            (int) $order->id_lang,
            'in_transit',
            $this->l('Package in transit'),
            $templateVars,
            $customer->email,
            $customer->firstname . ' ' . $customer->lastname,
            null,
            null,
            null,
            null,
            _PS_MAIL_DIR_,
            true,
            (int) $order->id_shop
        )) {
            return true;
        } else {
            return false;
        }
    }
}
