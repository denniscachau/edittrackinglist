<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

if (file_exists(_MODULE_DIR_ . 'edittrackinglist/vendor/autoload.php')) {
    include_once(_MODULE_DIR_ . 'edittrackinglist/vendor/autoload.php');
}

class Edittrackinglist extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'edittrackinglist';
        $this->tab = 'administration';
        $this->version = '1.0.4';
        $this->author = 'Ciren';
        $this->need_instance = 1;
        $this->module_key = 'b374606b796929290c938fb9ac7c718f';

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Edit the shipping directly from the list');
        $this->description = $this->l('Edit tracking number and carriers directly from the list');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.7.8');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (!parent::install()) {
            return false;
        }

        include dirname(__FILE__) . '/sql/install.php';

        $this->insertDb();

        if (version_compare(_PS_VERSION_, '1.7.7.0', '>=')) {
            $this->registerHook('actionOrderGridQueryBuilderModifier');
            $this->registerHook('actionOrderGridDefinitionModifier');
        } else {
            $this->registerHook('actionAdminOrdersListingFieldsModifier');
        }

        return $this->addTab() && $this->registerHook('backOfficeHeader');
    }

    protected function insertDb()
    {
        $sql = array();

        if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
            $position = 1;
        } else {
            $position = 0;
        }

        $sql[] = array(
            'name' => 'carriers',
            'name_display' => $this->l('Carriers'),
            'title' => $this->l('Carriers'),
            'active' => 1,
            'position' => $position
        );

        $sql[] = array(
            'name' => 'tracking',
            'name_display' => $this->l('Tracking number'),
            'title' => $this->l('Tracking'),
            'active' => 1,
            'position' => $position + 1
        );

        $sql[] = array(
            'name' => 'delivered',
            'name_display' => $this->l('Delivered'),
            'title' => $this->l('Delivered'),
            'active' => 1,
            'position' => $position + 2,
            'mail' => Db::getInstance()->getValue(
                "SELECT `send_email`
                FROM `" . _DB_PREFIX_ . "order_state`
                WHERE `id_order_state` = 5"
            )
        );

        foreach ($sql as $data) {
            Db::getInstance()->insert('edittrackinglist', $data, false, true, Db::INSERT_IGNORE);
        }
    }

    protected function addTab()
    {
        if (Tab::getIdFromClassName('AdminEditTrackingList') === false) {
            $tab = new Tab();
            $tab->class_name = 'AdminEditTrackingList';
            $tab->id_parent = (int)Tab::getIdFromClassName('AdminThemes');
            $tab->module = $this->name;
            $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->displayName;
            $tab->active = false;
            $tab->add();
        }

        if (version_compare(_PS_VERSION_, '1.7.7.0', '>=') &&
            Tab::getIdFromClassName('AdminEditTrackingListGrid') === false) {
            $tab = new Tab();
            $tab->class_name = 'AdminEditTrackingListGrid';
            $tab->id_parent = (int)Tab::getIdFromClassName('AdminThemes');
            $tab->module = $this->name;
            $tab->active = false;
            $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->displayName;
            $tab->add();
        }

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }

        include dirname(__FILE__) . '/sql/uninstall.php';

        return $this->removeTab();
    }

    private function removeTab()
    {
        if (version_compare(_PS_VERSION_, '1.7.1.0', '<')) {
            $ids_tab = array(
                (int)Tab::getIdFromClassName('AdminEditTrackingList'),
                (int)Tab::getIdFromClassName('AdminEditTrackingListGrid'),
            );

            foreach ($ids_tab as $id_tab) {
                if ($id_tab) {
                    $tab = new Tab($id_tab);
                    $tab->delete();
                }
            }
        }

        return true;
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $link = (new Link())->getAdminLink('AdminEditTrackingList', true);
        Tools::redirectAdmin($link);
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        $this->context->controller->addJquery();
        $this->context->controller->addJS($this->_path.'views/js/orders_list.js');

        $this->context->smarty->assign(array(
            'etl_link' => $this->context->link,
            'ajax_token_link' => Tools::getAdminTokenLite('AdminEditTrackingList'),
            'mail_methods' => Db::getInstance()->executeS(
                "SELECT `name`, `mail`
                    FROM `" . _DB_PREFIX_ . "edittrackinglist`"
            ),
            'mail_confirm' => $this->l('Do you want to send the e-mail with the changes?')
        ));

        return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/init_js.tpl');
    }

    public function hookActionAdminOrdersListingFieldsModifier($params)
    {
        if (!isset($params['select'])) {
            $params['select'] = '';
        }
        $params['select'] .= ", GROUP_CONCAT(DISTINCT
            CONCAT(`etl_order_carrier`.`id_order_carrier`, ',', `etl_order_carrier`.`id_carrier`)
            SEPARATOR ';;') AS 'etl_order_carrier_name'";
        $params['select'] .= ", GROUP_CONCAT(DISTINCT
            CONCAT(`etl_order_carrier`.`id_order_carrier`, ',', `etl_order_carrier`.`tracking_number`)
            SEPARATOR ';;') AS 'etl_order_carrier_tracking'";
        $params['select'] .= ", `a`.`id_cart` AS 'etl_orders_id_cart'";
        $params['select'] .= ", `a`.`id_address_delivery` AS 'etl_orders_id_address_delivery'";
        $params['select'] .= ", `os`.`shipped` AS 'etl_order_state_delivered'";
        $params['select'] .= ", `etl_order_carrier`.`id_carrier` AS 'etl_id_carrier'";
        $params['select'] .= ", `etl_carrier`.`name` AS 'etl_carrier_name'";

        if (!isset($params['join'])) {
            $params['join'] = '';
        }
        $params['join'] .= " LEFT JOIN `" . _DB_PREFIX_ . "order_carrier` `etl_order_carrier`
            ON `a`.`id_order` = `etl_order_carrier`.`id_order`";
        $params['join'] .= " LEFT JOIN `" . _DB_PREFIX_ . "carrier` `etl_carrier`
            ON `etl_order_carrier`.`id_carrier` = `etl_carrier`.`id_carrier`";

        $params['group_by'] = 'GROUP BY `a`.`id_order`';

        $configs = Db::getInstance()->executeS(
            "SELECT *
            FROM `" . _DB_PREFIX_ . "edittrackinglist`
            WHERE `active` = 1
            ORDER BY `position`"
        );

        if (isset($params['fields']['id_pdf'])) {
            $pdf = $params['fields']['id_pdf'];
            unset($params['fields']['id_pdf']);
        }

        foreach ($configs as $config) {
            switch ($config['name']) {
                case 'carriers':
                    $shop_name = str_replace(
                        array('#', ';'),
                        '',
                        Configuration::get('PS_SHOP_NAME')
                    );
                    $list = Db::getInstance()->executeS(
                        "SELECT `c`.`name` AS 'id_carrier', 
                        IF(c.name = '0', '" . pSQL($shop_name) . "', c.name) AS 'name'
                        FROM `" . _DB_PREFIX_ . "carrier` `c`
                        GROUP BY `c`.`name`"
                    );

                    $carrier_list = array();
                    foreach ($list as $item) {
                        $carrier_list[$item['id_carrier']] = $item['name'];
                    }
                    $params['fields']['etl_order_carrier_name'] = array(
                        'title' => $config['title'],
                        'callback' => 'printAjaxSelectField',
                        'callback_object' => $this,
                        'remove_onclick' => true,
                        'type' => 'select',
                        'list' => $carrier_list,
                        'filter_key' => 'etl_carrier!name'
                    );
                    break;

                case 'tracking':
                    $params['fields']['etl_order_carrier_tracking'] = array(
                        'title' => $config['title'],
                        'callback' => 'printAjaxTextField',
                        'callback_object' => $this,
                        'remove_onclick' => true,
                        'havingFilter' => true
                    );
                    break;

                case 'delivered':
                    $params['fields']['etl_order_state_delivered'] = array(
                        'title' => $config['title'],
                        'callback' => 'printDeliveredField',
                        'callback_object' => $this,
                        'remove_onclick' => true,
                        'type' => 'bool',
                        'filter_key' => 'os!shipped'
                    );
                    break;
            }
        }

        if (isset($pdf)) {
            $params['fields']['id_pdf'] = $pdf;
        }
    }

    /**
     * @param mixed[] $params
     */
    public function hookActionOrderGridQueryBuilderModifier($params)
    {
        $params['search_query_builder']->addSelect("GROUP_CONCAT(DISTINCT
            CONCAT(`etl_order_carrier`.`id_order_carrier`, ',', `etl_order_carrier`.`id_carrier`)
            SEPARATOR ';;') AS 'etl_order_carrier_name'");
        $params['search_query_builder']->addSelect("GROUP_CONCAT(DISTINCT
            CONCAT(`etl_order_carrier`.`id_order_carrier`, ',', `etl_order_carrier`.`tracking_number`)
            SEPARATOR ';;') AS 'etl_order_carrier_tracking'");
        $params['search_query_builder']->addSelect("`o`.`id_cart` AS 'etl_orders_id_cart'");
        $params['search_query_builder']->addSelect("`o`.`id_address_delivery` AS 'etl_orders_id_address_delivery'");
        $params['search_query_builder']->addSelect("`os`.`shipped` AS 'etl_order_state_delivered'");

        $params['search_query_builder']->leftJoin(
            'o',
            _DB_PREFIX_ . "order_carrier",
            'etl_order_carrier',
            '`o`.`id_order` = `etl_order_carrier`.`id_order`'
        );

        $params['search_query_builder']->addGroupBy('o.id_order');

        $filters = $params['search_criteria']->getFilters();

        if (!empty($filters)) {
            if (array_key_exists('etl_order_carrier_name', $filters)) {
                $params['search_query_builder']->andWhere(
                    "`etl_order_carrier`.`id_carrier` = " . (int)$filters['etl_order_carrier_name']
                );
            }
            if (array_key_exists('etl_order_carrier_tracking', $filters)) {
                $params['search_query_builder']->andHaving(
                    "`etl_order_carrier_tracking` LIKE '%" . pSQL($filters['etl_order_carrier_tracking']) . "%'"
                );
            }
            if (array_key_exists('etl_order_state_delivered', $filters)) {
                $params['search_query_builder']->andWhere(
                    "`os`.`shipped` = " . (int)$filters['etl_order_state_delivered']
                );
            }
        }
    }

    public function hookActionOrderGridDefinitionModifier($params)
    {
        $columns = $params['definition']->getColumns();
        $filters = $params['definition']->getFilters();

        $configs = Db::getInstance()->executeS(
            "SELECT *
            FROM `" . _DB_PREFIX_ . "edittrackinglist`
            WHERE `active` = 1
            ORDER BY `position`"
        );

        $filters->remove('new');

        foreach ($configs as $config) {
            switch ($config['name']) {
                case 'carriers':
                    $shop_name = str_replace(
                        array('#', ';'),
                        '',
                        Configuration::get('PS_SHOP_NAME')
                    );
                    $list = Db::getInstance()->executeS(
                        "SELECT `c`.`id_carrier`, IF(c.name = '0', '" . pSQL($shop_name) . "', c.name) AS 'name'
                        FROM `" . _DB_PREFIX_ . "carrier` `c`"
                    );

                    $carrier_list = array();
                    foreach ($list as $item) {
                        $carrier_list[$item['name']] = $item['id_carrier'];
                    }

                    $columns->addBefore(
                        'actions',
                        (new PrestaShop\Module\EditTrackingList\Grid\Column\CarriersColumn(
                            'etl_order_carrier_name'
                        ))
                            ->setName($config['title'])
                            ->setOptions([
                                'field' => 'etl_order_carrier_name',
                                'object' => $this
                            ])
                    );
                    $filters->add(
                        (new PrestaShop\PrestaShop\Core\Grid\Filter\Filter(
                            'etl_order_carrier_name',
                            Symfony\Component\Form\Extension\Core\Type\ChoiceType::class
                        ))->setTypeOptions(
                            array(
                                'required' => false,
                                'attr' => array(
                                    'placeholder' => $config['title'],
                                ),
                                'choices' => $carrier_list,
                                'translation_domain' => false
                            )
                        )->setAssociatedColumn('etl_order_carrier_name')
                    );
                    break;

                case 'tracking':
                    $columns->addBefore(
                        'actions',
                        (new PrestaShop\Module\EditTrackingList\Grid\Column\TrackingColumn(
                            'etl_order_carrier_tracking'
                        ))
                            ->setName($config['title'])
                            ->setOptions([
                                'field' => 'etl_order_carrier_tracking',
                            ])
                    );
                    $filters->add(
                        (new PrestaShop\PrestaShop\Core\Grid\Filter\Filter(
                            'etl_order_carrier_tracking',
                            Symfony\Component\Form\Extension\Core\Type\TextType::class
                        ))->setTypeOptions(
                            array(
                                'required' => false,
                                'attr' => array(
                                    'placeholder' => $config['title']
                                ),
                            )
                        )->setAssociatedColumn('etl_order_carrier_tracking')
                    );
                    break;

                case 'delivered':
                    $columns->addBefore(
                        'actions',
                        (new PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\ToggleColumn(
                            'etl_order_state_delivered'
                        ))
                            ->setName($config['title'])
                            ->setOptions([
                                'field' => 'etl_order_state_delivered',
                                'primary_field' => 'id_order',
                                'route' => 'admin_edit_tracking_list_toggle_delivered',
                                'route_param_name' => 'orderId'
                            ])
                    );

                    $filters->add(
                        (new PrestaShop\PrestaShop\Core\Grid\Filter\Filter(
                            'etl_order_state_delivered',
                            PrestaShopBundle\Form\Admin\Type\YesAndNoChoiceType::class
                        ))->setTypeOptions(
                            array(
                                'required' => false,
                                'attr' => array(
                                    'placeholder' => $config['title']
                                ),
                            )
                        )->setAssociatedColumn('etl_order_state_delivered')
                    );
                    break;
            }
        }
    }

    public function printDeliveredField($value, $tr)
    {
        $this->context->smarty->assign(array(
            'etl_value' => $value,
            'etl_id_order' => (int)$tr['id_order'],
            'etl_token' => Tools::getAdminTokenLite('AdminEditTrackingList')
        ));

        return $this->context->smarty->fetch(
            $this->getLocalPath() . 'views/templates/admin/boolean_delivered_field.tpl'
        );
    }

    public function getCarriers($tr)
    {
        $cart = new Cart((int)$tr['etl_orders_id_cart']);

        $actual_carrier = new Carrier($tr['etl_id_carrier']);

        $carriers = Carrier::getCarriersForOrder(
            Address::getZoneById((int)$tr['etl_orders_id_address_delivery']),
            Customer::getGroupsStatic((int)$cart->id_customer),
            $cart
        );

        foreach ($carriers as &$carrier) {
            if ($carrier['name'] == $actual_carrier->name) {
                $carrier['id_carrier'] = $actual_carrier->id;
                break;
            }
        }

        return $carriers;
    }

    public function printAjaxSelectField($value, $tr)
    {
        $values = explode(';;', $value);

        $carriers = $this->getCarriers($tr);

        $this->context->smarty->assign(array(
            'carriers' => $carriers
        ));

        $selects = "";
        foreach ($values as $value) {
            $ids = explode(',', $value, 2);

            $id_order_carrier = (int)$ids[0];
            $id_carrier = (int)$ids[1];

            $this->context->smarty->assign(array(
                'id_order_carrier' => $id_order_carrier,
                'id_carrier' => $id_carrier
            ));

            $selects .= $this->context->smarty->fetch(
                $this->getLocalPath() . 'views/templates/admin/select_carriers_field.tpl'
            );
        }

        return $selects;
    }

    public function printAjaxTextField($value)
    {
        $values = explode(';;', $value);

        $texts = "";
        foreach ($values as $value) {
            $ids = explode(',', $value, 2);

            $id_order_carrier = (int)$ids[0];
            $tracking = $ids[1];

            $this->context->smarty->assign(array(
                'tracking' => $tracking,
                'id_order_carrier' => $id_order_carrier
            ));

            $texts .= $this->context->smarty->fetch(
                $this->getLocalPath() . 'views/templates/admin/text_tracking_field.tpl'
            );
        }

        return $texts;
    }

    public function isUsingNewTranslationSystem()
    {
        return false;
    }
}
