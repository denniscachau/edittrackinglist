/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

document.addEventListener("DOMContentLoaded", function(){
    $('form').on('change', '.etl', function(e) {
        let send_mail = false;
        switch ($(this).data('type')) {
            case 'carriers':
                switch (etl_mail_method_carriers) {
                    case 1:
                        send_mail = true;
                        break;
                    case 2:
                        send_mail = confirm(etl_send_mail_confirm);
                        break;
                }
                break;
            case 'tracking':
                switch (etl_mail_method_tracking) {
                    case 1:
                        send_mail = true;
                        break;
                    case 2:
                        send_mail = confirm(etl_send_mail_confirm);
                        break;
                }
                break;
        }

        $.ajax({
            type: 'POST',
            url: admin_edittrackinglist_ajax_url,
            dataType: 'json',
            async : false,
            data: {
                controller : 'AdminEditTrackingList',
                action : $(this).data('action'),
                ajax : true,
                id_order_carrier : $(this).data('id_order_carrier'),
                value : $(this).val(),
                send_mail : (send_mail ? 1 : 0)
            },
            success: function(data) {
                if (data.success == 1) {
                    $.growl.notice({ title: "", message: update_success_msg });
                } else {
                    $.growl.error({ title: "", message: data.text });
                }
            },
            error: function() {
                $.growl.error({ title: "", message: "An error has occurred." });
            }
        });
    });

    $('#order_filter_form').on('click', '.ps-togglable-row', function(e) {
        if ($(this).text() !== 'check') {
            window.location = $(this).data('toggle-url');
        }
    })
})