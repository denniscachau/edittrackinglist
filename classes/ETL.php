<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class ETL extends ObjectModel
{
    /** @var string Name */
    public $id_edittrackinglist;
    public $name;
    public $name_display;
    public $title;
    public $active;
    public $position;
    public $mail;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'edittrackinglist',
        'primary' => 'id_edittrackinglist',
        'multilang' => false,
        'fields' => array(
            'id_edittrackinglist' => array(
                'type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => false
            ),
            'position' => array(
                'type' => self::TYPE_INT, 'required' => false
            ),
            'name' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => false
            ),
            'name_display' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => false
            ),
            'title' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => false
            ),
            'active' => array(
                'type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false
            ),
            'mail' => array(
                'type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false
            ),
        ),
    );

    public function updatePosition($way, $position)
    {
        $id_edittrackinglist = Configuration::get('id_edittrackinglist');

        if (!$res = Db::getInstance()->executeS(
            "SELECT `id_edittrackinglist`, `position`
            FROM `" . _DB_PREFIX_ . "edittrackinglist`
            ORDER BY `position`"
        )) {
            return false;
        }

        if (version_compare(_PS_VERSION_, '1.6.1', '<')) {
            $buffer_position = 1;
        } else {
            $buffer_position = 0;
        }

        foreach ($res as $columns) {
            if ((int)$columns['position'] != $buffer_position) {
                Db::getInstance()->execute(
                    "UPDATE `" . _DB_PREFIX_ . "edittrackinglist`
                    SET `position` = '" . (int)$buffer_position . "'
                    WHERE `id_edittrackinglist` = '" . (int)$columns['id_edittrackinglist'] . "'"
                );
            }
            $buffer_position++;
            if ((int)$columns['id_edittrackinglist'] == (int)$id_edittrackinglist) {
                $moved_columns = Db::getInstance()->getRow(
                    "SELECT `id_edittrackinglist`, `position`
                    FROM `" . _DB_PREFIX_ . "edittrackinglist`
                    WHERE `id_edittrackinglist` = '" . (int)$id_edittrackinglist . "'"
                );
            }
        }

        if (!isset($moved_columns) || !isset($position)) {
            return false;
        }
        // < and > statements rather than BETWEEN operator
        // since BETWEEN is treated differently according to databases
        return (Db::getInstance()->execute("
            UPDATE `" . _DB_PREFIX_ . "edittrackinglist`
            SET `position`= `position` " . ($way ? "- 1" : "+ 1") . "
            WHERE `position`
            " . ($way
                    ? "> " . (int)$moved_columns['position'] . " AND `position` <= " . (int)$position
                    : "< " . (int)$moved_columns['position']." AND `position` >= " . (int)$position . "
            "))
            && Db::getInstance()->execute('
            UPDATE `'._DB_PREFIX_.'edittrackinglist`
            SET `position` = '.(int)$position.'
            WHERE `id_edittrackinglist` = '.(int)$moved_columns['id_edittrackinglist']));
    }

    public function changePositions()
    {
        if (!$res = Db::getInstance()->executeS(
            "SELECT `id_edittrackinglist`, `position`
            FROM `" . _DB_PREFIX_ . "edittrackinglist`
            ORDER BY `position`"
        )) {
            return false;
        }

        if (version_compare(_PS_VERSION_, '1.6.1', '<')) {
            $buffer_position = 1;
        } else {
            $buffer_position = 0;
        }

        foreach ($res as $columns) {
            if ((int)$columns['position'] != $buffer_position) {
                if (!Db::getInstance()->execute(
                    "UPDATE `" . _DB_PREFIX_ . "edittrackinglist`
                    SET `position` = '" . (int)$buffer_position . "'
                    WHERE `id_edittrackinglist` = '" . (int)$columns['id_edittrackinglist'] . "'"
                )) {
                    return false;
                }
            }
            $buffer_position++;
        }
        return true;
    }

    public static function getLastPosition()
    {
        return Db::getInstance()->getValue(
            "SELECT IF(MAX(`position`) IS NULL, 0, MAX(`position`)+1)
            FROM `" . _DB_PREFIX_ . "edittrackinglist`"
        );
    }

    /**
     * @param Order $order
     * @param OrderCarrier $order_carrier
     *
     * @return bool
     */
    public static function refreshShippingCost($order, $order_carrier)
    {
        if (empty($order->id)) {
            return false;
        }

        if (!Configuration::get('PS_ORDER_RECALCULATE_SHIPPING')) {
            return true;
        }

        $fake_cart = new Cart((int) $order->id_cart);
        $new_cart = $fake_cart->duplicate();
        $new_cart = $new_cart['cart'];

        // assign order id_address_delivery to cart
        $new_cart->id_address_delivery = (int) $order->id_address_delivery;

        // assign id_carrier
        $new_cart->id_carrier = (int) $order->id_carrier;

        //remove all products : cart (maybe change in the meantime)
        foreach ($new_cart->getProducts() as $product) {
            $new_cart->deleteProduct((int) $product['id_product'], (int) $product['id_product_attribute']);
        }

        // add real order products
        foreach ($order->getProducts() as $product) {
            $new_cart->updateQty(
                $product['product_quantity'],
                (int) $product['product_id'],
                null,
                false,
                'up',
                0,
                null,
                true,
                true
            );
        }

        $base_total_shipping_tax_excl = (float)$new_cart->getPackageShippingCost(
            (int)$order_carrier->id_carrier,
            false,
            null
        );
        $base_total_shipping_tax_incl = (float)$new_cart->getPackageShippingCost(
            (int)$order_carrier->id_carrier,
            true,
            null
        );

        $order_carrier->shipping_cost_tax_excl = $base_total_shipping_tax_excl;
        $order_carrier->shipping_cost_tax_incl = $base_total_shipping_tax_incl;
        $order_carrier->update();

        $cost_total = Db::getInstance()->getRow(
            "SELECT SUM(`shipping_cost_tax_excl`) as 'shipping_cost_tax_excl', 
            SUM(`shipping_cost_tax_incl`) as 'shipping_cost_tax_incl'
            FROM `" . _DB_PREFIX_ . "order_carrier`
            WHERE `id_order` = " . (int) $order->id
        );

        $order->total_shipping_tax_excl = $cost_total['shipping_cost_tax_excl'];
        $order->total_shipping_tax_incl = $cost_total['shipping_cost_tax_incl'];
        $order->total_shipping = $cost_total['shipping_cost_tax_incl'];
        $order->total_paid_tax_excl =
            $order->total_products + $order->total_wrapping_tax_excl +
            $cost_total['shipping_cost_tax_excl'] - $order->total_discounts_tax_excl;
        $order->total_paid_tax_incl =
            $order->total_products_wt + $order->total_wrapping_tax_excl +
            $cost_total['shipping_cost_tax_incl'] - $order->total_discounts_tax_incl;
        $order->total_paid = $order->total_paid_tax_incl;
        $order->update();

        // remove fake cart
        $new_cart->delete();

        return true;
    }
}
