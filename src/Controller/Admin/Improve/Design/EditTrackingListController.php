<?php
/**
 * 2007-2020 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

namespace PrestaShop\Module\EditTrackingList\Controller\Admin\Improve\Design;

use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use PrestaShopBundle\Security\Annotation\AdminSecurity;
use PrestaShopBundle\Security\Annotation\ModuleActivated;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class EditTrackingList.
 *
 * @ModuleActivated(moduleName="edittrackinglist", redirectRoute="admin_module_manage")
 */
class EditTrackingListController extends FrameworkBundleAdminController
{
    public function indexAction()
    {
    }

    /**
     * Toggle category status.
     *
     * @AdminSecurity(
     *     "is_granted(['update'], request.get('_legacy_controller'))",
     *     message="You do not have permission to update this."
     * )
     *
     * @param int $orderId
     *
     * @return RedirectResponse
     */
    public function toggleDeliveredAction(int $orderId)
    {
        $id_order_state = 5;

        $order = new \Order($orderId);
        $order_state = new \OrderState($id_order_state);

        if (!\Validate::isLoadedObject($order_state)) {
            $this->addFlash(
                'error',
                'The new order status is invalid.'
            );
        } else {
            $current_order_state = $order->getCurrentOrderState();
            if ($current_order_state->id != $order_state->id) {
                // Create new OrderHistory
                $history = new \OrderHistory();
                $history->id_order = $order->id;
                $history->id_employee = (int) $this->getContext()->employee->id;

                $use_existings_payment = false;
                if (!$order->hasInvoice()) {
                    $use_existings_payment = true;
                }
                $history->changeIdOrderState((int) $order_state->id, $order, $use_existings_payment);

                $carrier = new \Carrier($order->id_carrier, $order->id_lang);
                $templateVars = array();
                if ($history->id_order_state == \Configuration::get('PS_OS_SHIPPING') && $order->shipping_number) {
                    $templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));
                }

                // Save all changes
                if ($history->addWithemail(true, $templateVars)) {
                    // synchronizes quantities if needed..
                    if (\Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                        foreach ($order->getProducts() as $product) {
                            if (\StockAvailable::dependsOnStock($product['product_id'])) {
                                \StockAvailable::synchronize($product['product_id'], (int) $product['id_shop']);
                            }
                        }
                    }
                    $this->addFlash(
                        'success',
                        $this->trans('Successful update.', 'Admin.Notifications.Success')
                    );
                } else {
                    $this->addFlash(
                        'error',
                        'An error occurred while changing order status, or we were unable to send an email to the customer.'
                    );
                }
            } else {
                $this->addFlash(
                    'error',
                    'The order has already been assigned this status.'
                );
            }
        }
        
        return $this->redirectToRoute('admin_orders_index');
    }
}
